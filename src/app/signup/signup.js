(function () {
   'use strict';


   angular.module('app').controller('signup',[signup]);

   function signup() {
      var vm = this;
      vm.activate = activate;



      activate();

      function activate() {


         vm.data = [
            "Game Designing",
            "Trucking",
            "Bookkeeping",
            "Dentist",
            "Registered Nurse",
            "Pharmacist",
            "Computer Systems Analyst",
            "Physician",
            "Database Administrator",
            "Software Developer",
            "Physical Therapist",
            "Web Developer",
            "Dental Hygienist",
            "Occupational Therapist",
            "Veterinarian",
            "Computer Programmer",
            "School Psychologist",
            "Physical Therapist Assistant",
            "Interpreter & Translator",
            "Mechanical Engineer",
            "Veterinary Technologist & Technician",
            "Epidemiologist",
            "IT Manager",
            "Market Research Analyst",
            "Computer Systems Administrator",
            "Respiratory Therapist",
            "Medical Secretary",
            "Civil Engineer",
            "Substance Abuse Counselor",
            "Speech-Language Pathologist",
            "Landscaper & Groundskeeper",
            "Cost Estimator",
            "Marriage & Family Therapist",
            "Medical Assistant",
            "Lawyer",
            "Accountant",
            "Compliance Officer"
         ];


         vm.gridData = new kendo.data.ObservableArray([
            { feature: "Disk", details: "256 MB" },
            { feature: "RAM", details: "16 GB" },
            { feature: "Monitor", details: "Thunderbolt" }
         ]);

         vm.gridColumns = [
            { field: "feature", title: "Feature" },
            { field: "details", title: "Details" }
         ];

         vm.update = function(){
            vm.gridData[0].set("details", "1TB");
         }


      }
   }
})();