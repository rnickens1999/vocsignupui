(function () {
   'use strict';


   angular.module('app')
      .controller('staticgridlocal',[staticgridlocal]);

   function staticgridlocal() {
      var vm = this;
      vm.activate = activate;



      activate();

      function activate() {


         vm.gridColumns = [
            { field: "visited_network", title: "Visited Network" },
            { field: "durration", title: "Durration" },
            { field: "home_network", title: "Home Network" },
            { field: "end_datetime", title: "End Datetime" },
            { field: "imsi", title: "IMSI" },
            { field: "access_point_name", title: "Access Point Name" },
            { field: "begin_datetiime", title: "Begin Datetiime" },
            { field: "visited_mno_ip", title: "Visited MNO IP" },
            { field: "visited_country", title: "Visited Country" },
            { field: "client_port", title: "Client Port" },
            { field: "visited_mcc_mnc", title: "Visited MCC MNC" },
            { field: "home_mno_ip", title: "Home MNO IP" },
            { field: "ecgi_eci", title: "ECGI/ECI" },
            { field: "msisdn", title: "MSISDN" },
            { field: "brand_model", title: "Brand Model" },
            { field: "pdn_address", title: "PDN Address" },
            { field: "end_point_type", title: "End Point Type" },
            { field: "ip_address", title: "IP Address" },
            { field: "pgw_ip_address", title: "PGW IP Address" },
            { field: "home_country", title: "Home Country" },
            { field: "imei", title: "IMEI" },
            { field: "teid", title: "TEID" },
            { field: "customer_name", title: "Customer Name" },
            { field: "eroor_code", title: "Error Code" },
            { field: "default_bearer", title: "Default Bearer" },
            { field: "irection", title: "Direction" }
         ];

         vm.gridData = new kendo.data.ObservableArray([{
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:37:00.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:37:00.0",
                "visited_mno_ip": "190.13.104.66",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.144",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:36:36.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:36:36.0",
                "visited_mno_ip": "190.13.104.66",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.129",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:37:55.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:37:55.0",
                "visited_mno_ip": "190.13.104.84",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.129",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:40:13.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:40:13.0",
                "visited_mno_ip": "190.13.104.84",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.149",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:43:19.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:43:19.0",
                "visited_mno_ip": "190.13.104.72",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.189",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:40:36.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:40:36.0",
                "visited_mno_ip": "190.13.104.72",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.161",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:37:00.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:37:00.0",
                "visited_mno_ip": "190.13.104.66",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.144",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:36:36.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:36:36.0",
                "visited_mno_ip": "190.13.104.66",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.129",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:37:55.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:37:55.0",
                "visited_mno_ip": "190.13.104.84",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.129",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:40:13.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:40:13.0",
                "visited_mno_ip": "190.13.104.84",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.149",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:43:19.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:43:19.0",
                "visited_mno_ip": "190.13.104.72",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.189",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }, {
                "visited_network": "Sotelgui sa (LAGUI)",
                "durration": 0,
                "home_network": "Sotelgui sa (LAGUI)",
                "end_datetime": "2016-03-06 21:40:36.0",
                "imsi": "206201005329911",
                "access_point_name": "GPRS.BASE.BE",
                "begin_datetiime": "2016-03-06 21:40:36.0",
                "visited_mno_ip": "190.13.104.72",
                "sap_id": "100030",
                "visited_country": "Guinea",
                "client_port": 2123,
                "visited_mcc_mnc": "6112",
                "home_mno_ip": "217.72.232.161",
                "ecgi_eci": "",
                "msisdn": 32484421875,
                "brand_model": "NOBRAND/NOMODEL",
                "pdn_address": "null",
                "end_point_type": "",
                "ip_address": "",
                "rat_type": "",
                "pgw_ip_address": "null",
                "home_country": "Guinea",
                "imei": "null",
                "teid": "",
                "customer_name": "BASE Company NV/SA - VP 096",
                "eroor_code": "SUCCESS",
                "default_bearer": "null",
                "tai_tac": "",
                "irection": "OUTBOUNDD"
             }]
         );




      }
   }
})();